package boundaryConditions;

import others.Grain;

public interface IBoundary {
    Grain calc(Grain grain, int width, int height);
}
