package generateTypes;

import others.Grain;

import java.util.Random;

public class RandomGenerate implements IGenerate {
    @Override
    public Grain[][] randomize(Grain[][] grains, int width, int height, int[] parameters) {
        int numberOfLivingGrains = parameters[0];
        int securityLock = numberOfLivingGrains*10;
        Random random = new Random();
        int x, y;
        while(numberOfLivingGrains>0&&securityLock>0){
            x = random.nextInt(width-1);
            y = random.nextInt(height-1);
            if(!grains[x][y].isAlive()){
                grains[x][y].setAlive(true);
                grains[x][y].randomColor();
                numberOfLivingGrains--;
            }
            securityLock--;
        }

        return grains;
    }
}
