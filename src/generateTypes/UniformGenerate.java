package generateTypes;

import others.Grain;

import java.util.Random;

public class UniformGenerate implements IGenerate {
    @Override
    public Grain[][] randomize(Grain[][] grains, int width, int height, int[] parameters) {
        int numberOfLivingGrains = parameters[0];
        int securityLock = numberOfLivingGrains*10;
        int percent = (int)(Math.ceil((100*numberOfLivingGrains)/(height*width)));
        int distance = 10;
        int j = 10;
        for(int i = 10; i < percent; i+=10){
            distance--;
            j+=10;
        }
        int x,y;
        x = 0;
        y = 0;
        int tmp1 = x;
        int tmp2 = y;
        while (numberOfLivingGrains>0&&securityLock>0){
            if(x>width-1){
                y+=distance;
                x = tmp1;
            }
            if(y>height-1){
                tmp1++;
                x = tmp1;
                y = tmp2;
            }
            if(tmp1 == 100){
                tmp1 = 0;
                x = tmp1;
                tmp2++;
            }

            if(!grains[x][y].isAlive()){
                numberOfLivingGrains--;
                grains[x][y].setAlive(true);
                grains[x][y].randomColor();
            }
            x+=distance;

            securityLock--;
        }

        return  grains;
    }
}
