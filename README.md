#Naiwny rozrost ziaren

Program na zajęcia "Modelowanie Wieloskalowe"

Program pozwala na przeprowadzenie symulacji rozrostu ziaren przy różnych warunkach brzegowych i typach sąsiedztwa.

Warunki brzegowe:

 - Periodyczne
 
 - Nieperiodyczne
 
Sąsiedztwa ziaren:

 - Pentagonalne
 
 - Moore'a
 
 - Von Neumann'a
 
 - Heksagonalne Lewe
 
 - Heksagonalne Prawe
 
 - Heksagonalne losowe
 
